<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        User::create([
            'name' => 'ario',
            'email' => 'ario@gmail.com',
            'password' => '123456',
            'create_by' => 1,
            'create_by_desc' => 'ario',
        ]);
    }
}
