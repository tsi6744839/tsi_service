<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::table('MS_DATAEXCEL', function (Blueprint $table) {
            $table->foreignId('id_document')->after('id')->constrained('MS_EXCEL')->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::table('MS_DATAEXCEL', function (Blueprint $table) {
            $table->dropForeign('MS_DATAEXCEL_id_document_foreign');
            $table->dropColumn('id_document');;
        });
    }
};
