<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\Excel;
use App\Models\DataExcel;
// use App\Exports\UserExport;
use App\Imports\ImportExcel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Maatwebsite\Excel\Facades\Excel as MaatExcel;
use Laravel\Lumen\Routing\Controller as BaseController;

class Controller extends BaseController
{
    // public function index()
    // {
    //     $user = User::all();

    //     return response()->json([
    //         'status' => true,
    //         'message' => 'Berhasil Menampilkan Data User',
    //         'data' => $user->map(function ($item) {
    //             return [
    //                 'nama' => $item->name,
    //                 'email' => $item->email
    //             ];
    //         })
    //     ], 200);
    // }

    public function upload(Request $request)
    {
        try {
            //validasi data masuk
            $this->validate($request, [
                'document' => 'required|max:2048|mimes:csv,xlsx,txt',
                'user_id' => 'required|exists:MS_USERS,id',
                'user_name' => 'required|exists:MS_USERS,name'
            ]);

            //get info file
            $file = $request->file('document');
            $fileName = $file->getClientOriginalName();
            $destinationPath = '../storage/document';

            //insert into DB
            Excel::create([
                'fileName' => $fileName,
                'filePath' => $destinationPath,
                'create_by' => $request->input('user_id'),
                'create_by_desc' => $request->input('user_name'),
            ]);

            //read isi data document kemudian insert ke dalam DB
            MaatExcel::import(new ImportExcel, $file);

            //pindah file ke storage
            $file->move($destinationPath, $fileName);

            //kirim respon berhasil
            return response()->json([
                'status' => true,
                'message' => 'Berhasil upload batch data. Data akan diproses secara background oleh sistem'
            ], 201);

            //kirim pesan error
        } catch (\Throwable $th) {
            return response()->json([
                'status' => false,
                'message' => 'Gagal upload batch data',
                'data' => $th->getMessage()
            ], 500);
        }
    }

    public function show()
    {
        $data = DataExcel::all();

        if ($data->isEmpty()) {
            return response()->json([
                'status' => true,
                'message' => 'Data Kosong',
                'data' => []
            ], 200);
        } else {
            return response()->json([
                'status' => true,
                'message' => 'Berhasil mengambil data excel',
                'data' => $data
            ], 200);
        }
    }

    public function register(Request $request)
    {

        $validatedData = $this->validate($request, [
            'name' => 'required|max:255',
            'email' => 'required|unique:MS_USERS|email:dns',
            'password' => 'required'
        ]);

        // Mengenkripsi password
        $validatedData['password'] = Hash::make($validatedData['password']);

        //memasukkan data registrasi ke database
        $user = User::create([
            'name' => $validatedData['name'],
            'email' => $validatedData['email'],
            'password' => $validatedData['password'],
            'create_by' => 1,
            'create_by_desc' => 'ario'
        ]);

        return response()->json([
            'status' => true,
            'message' => 'Berhasil create user',
            'data' => $user
        ], 201);
    }

    // public function export()
    // {
    //     return MaatExcel::download(new UserExport, 'users.xlsx');
    // }
}
