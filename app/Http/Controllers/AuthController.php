<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Laravel\Lumen\Routing\Controller as BaseController;

class AuthController extends BaseController
{
    public function register(Request $request)
    {
        $validatedData = $this->validate($request, [
            'name' => 'required|max:255',
            'email' => 'required|unique:MS_USERS|email:dns',
            'password' => 'required'
        ]);

        // Mengenkripsi password
        $validatedData['password'] = Hash::make($validatedData['password']);

        //memasukkan data registrasi ke database
        $user = User::create([
            'name' => $validatedData['name'],
            'email' => $validatedData['email'],
            'password' => $validatedData['password'],
            'create_by' => 1,
            'create_by_desc' => 'ario'
        ]);

        return response()->json([
            'status' => true,
            'message' => 'Berhasil create user',
            'data' => $user
        ], 201);
    }
}
