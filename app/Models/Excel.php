<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Excel extends Model
{
    protected $table = 'MS_EXCEL';

    protected $guarded = [
        'id'
    ];
}
