<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DataExcel extends Model
{
    protected $table = 'MS_DATAEXCEL';

    protected $guarded = [
        'id'
    ];
}
