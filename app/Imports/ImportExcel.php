<?php

namespace App\Imports;

use App\Models\DataExcel;
use App\Models\Excel;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

class ImportExcel implements ToModel, WithHeadingRow
{
    /**
     * @param Collection $collection
     */
    public function model(array $row)
    {
        //get data utama
        $data = Excel::all()->last();

        return new DataExcel([
            'id_document' => $data->id,
            'nama'     => $row['nama'],
            'email'    => $row['email'],
            'alamat' => $row['alamat'],
            'pekerjaan' => $row['pekerjaan'],
            //untuk handle format date dari excel menjadi sesuai database.
            'tgl_lahir' => \PhpOffice\PhpSpreadsheet\Shared\Date::excelToDateTimeObject($row['tgl_lahir']),
            'create_by' => $data->create_by,
            'create_by_desc' => $data->create_by_desc,
        ]);
    }
}
